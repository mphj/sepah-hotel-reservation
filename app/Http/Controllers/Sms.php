<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class Sms extends Controller
{
    //


    public static function send_msg($phone, $msg)
    {
        $url = env('SMS_HOST');
        $rcpt_nm = array($phone);
        $param = array
                    (
                        'uname'=>env('SMS_USERNAME'),
                        'pass'=>env('SMS_PASSWORD'),
                        'from'=>'+98100009',
                        'message'=>$msg,
                        'to'=>json_encode($rcpt_nm),
                        'op'=>'send'
                    );
                    
        $handler = curl_init($url);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handler, CURLOPT_POSTFIELDS, $param);       
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $response2 = curl_exec($handler);
        
        $response2 = json_decode($response2);
        return $response2;
    }
}
