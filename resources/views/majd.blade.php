
<!DOCTYPE html>
<html>
  <head>
    <title>ستاد اسکان ندسا</title>
    <link rel="icon" type="image/jpeg" href="/imam-1.jpg">
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/sweetalert.css')}}"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="{{asset('css/persian-datepicker.min.css')}}"  media="screen,projection"/>
    <meta charset="utf-8">
    <!--Let browser know website is optimized for mobile-->
    <!--meta name="viewport" content="width=device-width, initial-scale=1.0"/-->


    <style type="text/css">
@font-face {
font-family: 'Lato';
font-style: normal;
src: local('Lato Regular'), local('Lato-Regular'), url(font/IRANSansWeb_Light.woff2) format('woff2');
}
/* latin */
@font-face {
font-family: 'Lato';
font-style: normal;
src: local('Lato Regular'), local('Lato-Regular'), url(font/IRANSansWeb_Light.woff2) format('woff2');
}
  html{
    height: 100%;
  }
  body{
    height: 100%;
    font-family: 'Lato', sans-serif;
    background: blue;
    background: -webkit-linear-gradient(right bottom, blue, white, green);
    background: -o-linear-gradient(bottom left, blue, white, green);
    background: -moz-linear-gradient(bottom left, blue, white, green);
    background: linear-gradient(to bottom left, blue, white, green);
    background-size: cover;
    background-position: 50% 50%;
    background-attachment: fixed;
  }
  *{
    font-family: 'Lato', sans-serif;
  }
      .brand-logo{
        margin-left:20px;
      }
      .parallax img{
        /*filter: blur(5px);*/
      }
      main{
        position: relative;
        top:-64px;
      }
      nav{
        transition: 0.3s;
      }
      nav.no-shadow{
        box-shadow: none;
      }
      #index-banner{
        z-index: 0;
        height: auto !important;
        overflow-y: hidden;
      }
      #index-banner > .section{
        width:100%;
        margin-top: 70px;
      }
      #index-banner h1.header{
        text-shadow: 0px 7px 15px black;
      }

      .nav-wrapper a.brand-logo{
        font-size: 1.7rem;
        margin-top: -10px;
        margin-right: 8px;
      }
      nav{
        height: 80px;
        background: linear-gradient(#555, transparent);
      }
      .main-card{
        border-top: 4px purple solid;
      }


      .dropdown-content{
        height: 300px;
      }

      #pay-btn{
        transition: 0.3s;
      }
      #pay-btn:focus{
        color: yellow !important;
      }
    </style>
  </head>

  <body>

  <div class="navbar-fixed">
    <nav class="transparent no-shadow">
  <div class="nav-wrapper">
    <a href="#!" class="brand-logo right valign-wrapper" style="margin-top:7px;"><img src="imam-1.jpg" class="right align" width="70" height="70" style="margin-top:-3px;margin-right:-5px;border-radius:50%;margin-left: 5px;"/>ستاد خیرین آموزشگاه های مناطق محروم جنوبی دریایی</a>
    <ul class="left hide-on-med-and-down">
      <li class="waves-effect waves-light"><a><i class="material-icons">person</i></a></li>
      <li class="waves-effect waves-light"><a href="#"><i class="material-icons">info</i></a></li>
      <li class="waves-effect waves-light"><a href="#"><i class="material-icons">help</i></a></li>
    </ul>
  </div>
</nav>
</div>



<main>
<div id="index-banner" class="parallax-container">
  <div class="section no-pad-bot valign">
    <div class="container">
    <div class="main-card" style="width:100%;border-radius:3px 3px 0px 0px;margin-bottom:-8px;z-index:999;position:relative;"></div>

















        <div class="row" id="main-info">
<div class="col s12 m12 l12">
<div class="card" id="first-card">
<div class="card-content">









  <div class="row" dir="rtl" style="margin-bottom: -30px;">
    <form class="col s12 m12 l12">
      <h4 style="width: 100%;text-align: right; border-bottom: 2px #9e9e9e solid;margin-top: 0px;padding-bottom: 10px;" class="orange-text">سامانه حمایت</h4>
      <span>لطفا جهت حمایت از فرم زیر اقدام بفرمایید</span><br><br>
      <div class="row">
        <div class="input-field col s4">
        <div style="position:absolute;left:20px;color:white;padding:5px 10px;top: 30px;border-radius: 0px 0px 5px 5px;transition: 0.3s;" class="blue z-depth-3">
        <span id="time-duration">صفر تومان</span>
        </div>
          <input id="price" type="text" style="direction: ltr; text-align: center;" data-next="#pay-btn">
          <label for="price">مبلغ</label>
        </div>
        <div class="input-field col s4">
          <input id="phone" type="text" data-next="#price">
          <label for="phone">شماره تلفن - اختیاری</label>
        </div>

        <div class="input-field col s4">
          <input id="persons" type="text" data-next="#phone">
          <label for="persons">نام و نام خانوادگی - اختیاری</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 center-align">
          <button class="waves-effect waves-light btn btn-large green auto-focus" id="pay-btn" data-next="#persons" style="z-index:0;"><i class="material-icons right">payment</i>پرداخت</button>
        </div>
      </div>
    </form>
  </div>





</div>
</div>
</div>
</div>

    </div>
  </div>
</div>



</main>


<div id="loader" style="position:fixed;top:0px;width:100%;height:100%;background:rgba(0, 0, 0, 0.5);z-index:9999;display: none;">
<div class="sk-fading-circle">
  <div class="sk-circle1 sk-circle"></div>
  <div class="sk-circle2 sk-circle"></div>
  <div class="sk-circle3 sk-circle"></div>
  <div class="sk-circle4 sk-circle"></div>
  <div class="sk-circle5 sk-circle"></div>
  <div class="sk-circle6 sk-circle"></div>
  <div class="sk-circle7 sk-circle"></div>
  <div class="sk-circle8 sk-circle"></div>
  <div class="sk-circle9 sk-circle"></div>
  <div class="sk-circle10 sk-circle"></div>
  <div class="sk-circle11 sk-circle"></div>
  <div class="sk-circle12 sk-circle"></div>
</div>
<h4 style="width: 100%;text-align: center;color: white;">به باجه ی پرداخت منتقل میشوید</h4>
</div>

<style type="text/css">
.sk-fading-circle {
  margin: 25% auto 0px auto;
  width: 80px;
  height: 80px;
  position: relative;
}

.sk-fading-circle .sk-circle {
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
}

.sk-fading-circle .sk-circle:before {
  content: '';
  display: block;
  margin: 0 auto;
  width: 15%;
  height: 15%;
  background-color: white;
  border-radius: 100%;
  -webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
          animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
}
.sk-fading-circle .sk-circle2 {
  -webkit-transform: rotate(30deg);
      -ms-transform: rotate(30deg);
          transform: rotate(30deg);
}
.sk-fading-circle .sk-circle3 {
  -webkit-transform: rotate(60deg);
      -ms-transform: rotate(60deg);
          transform: rotate(60deg);
}
.sk-fading-circle .sk-circle4 {
  -webkit-transform: rotate(90deg);
      -ms-transform: rotate(90deg);
          transform: rotate(90deg);
}
.sk-fading-circle .sk-circle5 {
  -webkit-transform: rotate(120deg);
      -ms-transform: rotate(120deg);
          transform: rotate(120deg);
}
.sk-fading-circle .sk-circle6 {
  -webkit-transform: rotate(150deg);
      -ms-transform: rotate(150deg);
          transform: rotate(150deg);
}
.sk-fading-circle .sk-circle7 {
  -webkit-transform: rotate(180deg);
      -ms-transform: rotate(180deg);
          transform: rotate(180deg);
}
.sk-fading-circle .sk-circle8 {
  -webkit-transform: rotate(210deg);
      -ms-transform: rotate(210deg);
          transform: rotate(210deg);
}
.sk-fading-circle .sk-circle9 {
  -webkit-transform: rotate(240deg);
      -ms-transform: rotate(240deg);
          transform: rotate(240deg);
}
.sk-fading-circle .sk-circle10 {
  -webkit-transform: rotate(270deg);
      -ms-transform: rotate(270deg);
          transform: rotate(270deg);
}
.sk-fading-circle .sk-circle11 {
  -webkit-transform: rotate(300deg);
      -ms-transform: rotate(300deg);
          transform: rotate(300deg); 
}
.sk-fading-circle .sk-circle12 {
  -webkit-transform: rotate(330deg);
      -ms-transform: rotate(330deg);
          transform: rotate(330deg); 
}
.sk-fading-circle .sk-circle2:before {
  -webkit-animation-delay: -1.1s;
          animation-delay: -1.1s; 
}
.sk-fading-circle .sk-circle3:before {
  -webkit-animation-delay: -1s;
          animation-delay: -1s; 
}
.sk-fading-circle .sk-circle4:before {
  -webkit-animation-delay: -0.9s;
          animation-delay: -0.9s; 
}
.sk-fading-circle .sk-circle5:before {
  -webkit-animation-delay: -0.8s;
          animation-delay: -0.8s; 
}
.sk-fading-circle .sk-circle6:before {
  -webkit-animation-delay: -0.7s;
          animation-delay: -0.7s; 
}
.sk-fading-circle .sk-circle7:before {
  -webkit-animation-delay: -0.6s;
          animation-delay: -0.6s; 
}
.sk-fading-circle .sk-circle8:before {
  -webkit-animation-delay: -0.5s;
          animation-delay: -0.5s; 
}
.sk-fading-circle .sk-circle9:before {
  -webkit-animation-delay: -0.4s;
          animation-delay: -0.4s;
}
.sk-fading-circle .sk-circle10:before {
  -webkit-animation-delay: -0.3s;
          animation-delay: -0.3s;
}
.sk-fading-circle .sk-circle11:before {
  -webkit-animation-delay: -0.2s;
          animation-delay: -0.2s;
}
.sk-fading-circle .sk-circle12:before {
  -webkit-animation-delay: -0.1s;
          animation-delay: -0.1s;
}

@-webkit-keyframes sk-circleFadeDelay {
  0%, 39%, 100% { opacity: 0; }
  40% { opacity: 1; }
}

@keyframes sk-circleFadeDelay {
  0%, 39%, 100% { opacity: 0; }
  40% { opacity: 1; } 
}
</style>

<input type="hidden" id="f_place">
<input type="hidden" id="f_place_title">
<input type="hidden" id="f_nums" value="1">

    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="{{asset('jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/materialize.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/persian-date.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/persian-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/wordifyfa.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.9/jquery.mask.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {

        $("input, button").keydown(function(event) {
          if (event.keyCode == 9){
            event.preventDefault();
            $(this).blur();
            $($(this).attr('data-next')).focus();
          }
        });
        $("#price").mask("000,000,000,000,000"); 
        $("#price").keyup(function(event) {
          var a = $(this).val();
          var a = a.replace(/,/g, '');
          if (a == "" || a == null || a == "0"){
            $("#time-duration").html('صفر تومان');
            return;            
          }
          $("#time-duration").html(wordifyfa(parseInt(a)) + " " + "تومان");
        });
      });
    </script>
  </body>
</html>
